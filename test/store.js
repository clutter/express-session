/* globals before, after, describe, it */
/*eslint func-names: ["error", "never"]*/
/*eslint no-magic-numbers: "off"*/
/*eslint  max-nested-callbacks: ["error", 6]*/

"use strict";

const assert = require("assert");
const Identity = require("../lib/Identity.js");
const Store = require("../lib/Store.js");

// test data
var id1 = new Identity("tid1", "uid1");
var id11 = new Identity("tid1", "uid1");
var id12 = new Identity("tid1", "uid2");
var id2 = new Identity("tid2", "uid2");
var data1 = {
    "ying": "yang"
};
var data2 = {
    "ping": "pong"
};

var store;

before(function (done) {
    store = new Store();
    done();
});

describe("Store", function () {
    it("should be a class", function (done) {
        assert.strictEqual(typeof Store, "function");
        done();
    });
    it("should set data", function (done) {
        store.set(id1, data1, null, function (err) {
            assert.ok(!err);
            store.set(id2, data2, null, function (err1) {
                assert.ok(!err1);
                done();
            });
        });
    });
    it("should get data", function (done) {
        store.get(id2, function (err, val2) {
            assert.ok(!err);
            assert.deepStrictEqual(val2, data2);
            store.get(id1, function (err1, val1) {
                assert.ok(!err1);
                assert.deepStrictEqual(val1, data1);
                done();
            });
        });
    });
    it("should destroy data", function (done) {
        store.destroy(id1, function (err) {
            assert.ok(!err);
            store.get(id2, function (err1, val2) {
                assert.ok(!err1);
                assert.deepStrictEqual(val2, data2);
                done();
            });
        });
    });
    it("should error on reading destroyed data", function (done) {
        store.get(id1, function (err) {
            assert.ok(err instanceof Error);
            done();
        });
    });
    it("should overwrite data on set", function (done) {
        store.set(id2, data1, null, function (err) {
            assert.ok(!err);
            store.get(id2, function (err1, val1) {
                assert.ok(!err1);
                assert.deepStrictEqual(val1, data1);
                done();
            });
        });
    });
    it("should expire data by maxAge", function (done) {
        function checkExpiry() {
            store.get(id2, function (err) {
                assert.ok(err instanceof Error);
                done();
            });
        }
        store.set(id2, data2, 50, function (err) {
            assert.ok(!err);
            store.get(id2, function (err1, val2) {
                assert.ok(!err1);
                assert.deepStrictEqual(val2, data2);
                setTimeout(checkExpiry, 75);
            });
        });
    });
    it("should set data for same identity", function (done) {
        store.set(id1, data1, null, function (err) {
            assert.ok(!err);
            store.set(id11, data2, null, function (err1) {
                assert.ok(!err1);
                store.set(id2, data2, null, function (err2) {
                    assert.ok(!err2);
                    done();
                });
            });
        });
    });
    it("should get data for same identity", function (done) {
        store.get(id1, function (err, val1) {
            assert.ok(!err);
            assert.deepStrictEqual(val1, data1);
            store.get(id11, function (err1, val11) {
                assert.ok(!err1);
                assert.deepStrictEqual(val11, data2);
                done();
            });
        });
    });
    it("should destroy all user data", function (done) {
        store.destroyAllUserSessions("uid1", function (err) {
            assert.ok(!err);
            store.get(id1, function (err1) {
                assert.ok(err1 instanceof Error);
                store.get(id11, function (err2) {
                    assert.ok(err2 instanceof Error);
                    store.get(id2, function (err3, val2) {
                        assert.ok(!err3);
                        assert.deepStrictEqual(val2, data2);
                        done();
                    });
                });
            });
        });
    });
    it("should set data for different users of same tenant", function (done) {
        store.set(id11, data1, null, function (err) {
            assert.ok(!err);
            store.set(id12, data2, null, function (err1) {
                assert.ok(!err1);
                done();
            });
        });
    });
    it("should get data for different users of same tenant", function (done) {
        store.get(id11, function (err, val11) {
            assert.ok(!err);
            assert.deepStrictEqual(val11, data1);
            store.get(id12, function (err1, val12) {
                assert.ok(!err1);
                assert.deepStrictEqual(val12, data2);
                done();
            });
        });
    });
    it("should destroy all tenant data", function (done) {
        store.destroyAllTenantSessions("tid1", function (err) {
            assert.ok(!err);
            store.get(id11, function (err1) {
                assert.ok(err1 instanceof Error);
                store.get(id12, function (err2) {
                    assert.ok(err2 instanceof Error);
                    store.get(id2, function (err3, val2) {
                        assert.ok(!err3);
                        assert.deepStrictEqual(val2, data2);
                        done();
                    });
                });
            });
        });
    });
    it("should destroy all data", function (done) {
        store.destroyAllSessions(function (err) {
            assert.ok(!err);
            store.get(id2, function (err1) {
                assert.ok(err1 instanceof Error);
                done();
            });
        });
    });
});

after(function (done) {
    done();
});
