/* globals before, after, describe, it */
/*eslint func-names: ["error", "never"]*/
/*eslint no-magic-numbers: "off"*/

"use strict";

const assert = require("assert");
const Cookie = require("../lib/Cookie.js");

var cookie;

before(function (done) {
    done();
});

describe("Cookie", function () {
    it("should be a class", function (done) {
        assert.strictEqual(typeof Cookie, "function");
        done();
    });
    it("should set defaults", function (done) {
        cookie = new Cookie({
            "secret": "sauce"
        });
        assert.strictEqual(cookie.name, "express.sid");
        assert.strictEqual(cookie.signed, true);
        assert.strictEqual(cookie.maxAge, 3600000);
        done();
    });
    it("should discard unknown options", function (done) {
        cookie = new Cookie({
            "secret": "sauce",
            "ying": "yang"
        });
        assert.strictEqual(cookie.ying, undefined);
        done();
    });
});

after(function (done) {
    done();
});
