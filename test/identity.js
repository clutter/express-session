/* globals before, after, describe, it */
/*eslint func-names: ["error", "never"]*/
/*eslint no-magic-numbers: "off"*/

"use strict";

const assert = require("assert");
const Identity = require("../lib/Identity.js");

var id;

before(function (done) {
    done();
});

describe("Identity", function () {
    it("should be a class", function (done) {
        assert.strictEqual(typeof Identity, "function");
        done();
    });
    it("should set defaults", function (done) {
        id = new Identity();
        assert.strictEqual(id.tid, "");
        assert.strictEqual(id.uid, "");
        assert.ok(id.sid.match(/^.{24,}$/));
        done();
    });
    it("should generate sid", function (done) {
        assert.ok(Identity.generateSid().match(/^.{24,}$/));
        done();
    });
    it("should renew sid", function (done) {
        let sid;

        id = new Identity("tid1", "uid1");
        assert.strictEqual(id.tid, "tid1");
        assert.strictEqual(id.uid, "uid1");
        assert.ok(id.sid.match(/^.{24,}$/));
        sid = id.sid;
        id.updateTidUid("tid2", "uid2");
        assert.strictEqual(id.tid, "tid2");
        assert.strictEqual(id.uid, "uid2");
        assert.ok(id.sid.match(/^.{24,}$/));
        assert.ok(sid !== id.sid);
        sid = id.sid;
        id.updateUid("uid3");
        assert.strictEqual(id.tid, "tid2");
        assert.strictEqual(id.uid, "uid3");
        assert.ok(id.sid.match(/^.{24,}$/));
        assert.ok(sid !== id.sid);
        sid = id.sid;
        id.updateTid("tid4");
        assert.strictEqual(id.tid, "tid4");
        assert.strictEqual(id.uid, "uid3");
        assert.ok(id.sid.match(/^.{24,}$/));
        assert.ok(sid !== id.sid);
        sid = id.sid;
        id.renewSid();
        assert.strictEqual(id.tid, "tid4");
        assert.strictEqual(id.uid, "uid3");
        assert.ok(id.sid.match(/^.{24,}$/));
        assert.ok(sid !== id.sid);
        done();
    });
    it("should provide a string via toString()", function (done) {
        id = new Identity();
        assert.ok(id.toString().constructor === "".constructor);
        done();
    });
    it("should be able to regenerate identity fromString()", function (done) {
        let nid;

        id = new Identity("tid", "uid");
        nid = Identity.fromString(id.toString());
        assert.deepStrictEqual(id, nid);
        done();
    });
});

after(function (done) {
    done();
});
