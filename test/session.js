/* globals before, after, describe, it */
/*eslint func-names: ["error", "never"]*/
/*eslint no-magic-numbers: "off"*/
/*eslint  max-nested-callbacks: ["error", 5]*/

"use strict";

const assert = require("assert");
const Identity = require("../lib/Identity.js");
const Cookie = require("../lib/Cookie.js");
const Store = require("../lib/Store.js");
const Session = require("../lib/Session.js");

// test data
var id;
var cookie;
var store;
var session;

before(function (done) {
    id = new Identity();
    cookie = new Cookie({
        "secret": "sauce"
    });
    store = new Store();
    session = new Session(id, cookie, store);
    done();
});

describe("Session", function () {
    it("should be a class", function (done) {
        assert.strictEqual(typeof Session, "function");
        done();
    });
    it("should have no enumerable properties", function (done) {
        assert.strictEqual(Object.keys(session).length, 0);
        done();
    });
    it("should have id/cookie/store properties", function (done) {
        assert.ok(session.id);
        assert.ok(session.cookie);
        assert.ok(session.store);
        done();
    });
    it("should save data", function (done) {
        session.ying = "yang";
        session.key = {
            "ping": "pong"
        };
        session.save(function (err) {
            assert.ok(!err);
            store.get(id, function (err1, val1) {
                assert.ok(!err1);
                assert.deepEqual(session, val1);
                done();
            });
        });
    });
    it("should load data", function (done) {
        Object.keys(session).forEach(function (key) {
            delete session[key];
        });
        session.load(function (err) {
            assert.ok(!err);
            store.get(id, function (err1, val1) {
                assert.ok(!err1);
                assert.deepEqual(session, val1);
                done();
            });
        });
    });
    it("should destroy data", function (done) {
        session.destroy(function (err) {
            assert.ok(!err);
            store.get(id, function (err1) {
                assert.ok(err1 instanceof Error);
                assert.deepEqual(session, {});
                done();
            });
        });
    });
});

after(function (done) {
    done();
});
