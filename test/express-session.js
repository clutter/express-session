/* globals before, after, describe, beforeEach, afterEach, it */
/*eslint func-names: ["error", "never"]*/
/*eslint no-magic-numbers: "off"*/

"use strict";

const assert = require("assert");
const http = require("http");
const express = require("express");
const supertest = require("supertest");
const expressSession = require("../index");

const app = express();
const opts = {
    "cookie": {
        "name": "test.sid",
        "maxAge": 1000
    }
};
var server;
var req1;
var req2;
var val1;
var val2;

app.use(expressSession.getMiddleware(opts));
app.get("/cookie",
    function (req, res) {
        res.end("OK");
    }
);
app.get("/renew",
    function (req, res) {
        req.session.id.renewSid();
        res.end("OK");
    }
);
app.post("/value",
    function (req, res) {
        req.session.value = 1;
        res.end();
    }
);
app.put("/value",
    function (req, res) {
        req.session.value++;
        res.end();
    }
);
app.get("/value",
    function (req, res) {
        res.json({
            "value": req.session.value || 0
        });
    }
);

server = http.createServer(app);
req1 = supertest.agent(server);
req2 = supertest.agent(server);

function getCookieString(res) {
    return res.headers["set-cookie"][0].match(/^test.sid=(.+?);/)[1];
}

before(function (done) {
    done();
});

describe("express-session", function () {
    beforeEach(function (done) {
        server.listen(9090, done);
    });
    afterEach(function (done) {
        server.close(done);
    });
    it("should be a middleware", function (done) {
        assert.ok(typeof expressSession.getMiddleware === "function");
        assert.ok(typeof expressSession.getMiddleware() === "function");
        assert.ok(expressSession.getMiddleware().length === 3);
        done();
    });
    it("should set cookie", function (done) {
        req1
            .get("/cookie")
            .expect(200, function (err, res) {
                assert.ok(!err);
                val1 = getCookieString(res);
                done();
            });
    });
    it("should set different cookie for different request", function (done) {
        req2
            .get("/cookie")
            .expect(200, function (err, res) {
                assert.ok(!err);
                val2 = getCookieString(res);
                assert.notEqual(val1, val2);
                done();
            });
    });
    it("should renew cookie", function (done) {
        req2
            .get("/renew")
            .expect(200, function (err, res) {
                assert.ok(!err);
                assert.notEqual(val2, getCookieString(res));
                done();
            });
    });
    it("should not GET/read value before creating", function (done) {
        req1
            .get("/value")
            .expect(200, {
                "value": 0
            }, done);
    });
    it("should POST/set value", function (done) {
        req1
            .post("/value")
            .expect(200, done);
    });
    it("should GET/read value after creating", function (done) {
        req1
            .get("/value")
            .expect(200, {
                "value": 1
            }, done);
    });
    it("should PUT/increment value", function (done) {
        req1
            .put("/value")
            .expect(200, done);
    });
    it("should GET/read value after increment", function (done) {
        req1
            .get("/value")
            .expect(200, {
                "value": 2
            }, done);
    });
    it("should not GET/read value after cookie-expiry", function (done) {
        setTimeout(function () {
            req1
                .get("/value")
                .expect(200, {
                    "value": 0
                }, done);
        }, 1500);
    });
});

after(function (done) {
    done();
});
