"use strict";

const cookieParser = require("cookie-parser");
const onHeaders = require("on-headers");
const lib = require("./lib");

function getMiddleware(options) {
    let opts = options || {};
    let Identity = opts.Identity || lib.Identity;
    let store = opts.store || new lib.Store();
    let cookie = new lib.Cookie(opts.cookie);
    let cookieParserMiddleware = cookieParser(cookie.secret, cookie);

    return function middleware(req, res, next) {
        cookieParserMiddleware(req, res, function expressSession() {
            let id;

            if (arguments[0]) { // eslint-disable-line no-magic-numbers
                return next.apply(null, arguments);
            }
            if (req.signedCookies[cookie.name]) {
                id = Identity.fromString(req.signedCookies[cookie.name]);
            } else {
                id = new Identity();
            }
            onHeaders(res, () => res.cookie(cookie.name, id.toString(), cookie));
            req.session = new lib.Session(id, cookie, store);
            res.on("finish", () => req.session.save());
            req.session.load(() => next());

            return null;
        });
    };
}

exports.Identity = lib.Identity;
exports.Store = lib.Store;
exports.getMiddleware = getMiddleware;
