"use strict";

// refer: https://expressjs.com/en/4x/api.html#res.cookie
// plus: name & secret
// fixed: signed = true;
// defaults: name = express.sid; secret = "hot sauce"; maxAge = 1-hour;

const opts = ["name", "secret", "domain", "encode", "expires", "httpOnly", "maxAge", "path", "secure", "sameSite"];
const ONE_HOUR_MS = 3600000;

class Cookie {
    constructor(options) {
        let self = this;

        this.name = "express.sid";
        this.secret = "hot sauce";
        this.signed = true;
        this.maxAge = ONE_HOUR_MS;
        if (options) {
            opts.forEach(function eachKey(key) {
                if (options[key]) {
                    self[key] = options[key];
                }
            });
        }
    }
}

module.exports = Cookie;
