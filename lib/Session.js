"use strict";

function noOp() {} // eslint-disable-line no-empty-function

function clone(obj) {
    return Object.assign({}, obj);
}

function overwriteSession(that, session) {
    Object.keys(that).forEach(function eachKey(key) {
        delete that[key];
    });
    Object.assign(that, session);
}

class Session {
    constructor(id, cookie, store) {
        let _id = id;
        let _cookie = cookie;
        let _store = store;

        Object.defineProperty(this, "id", {
            "value": _id,
            "writable": false,
            "configurable": false,
            "enumerable": false
        });
        Object.defineProperty(this, "cookie", {
            "value": _cookie,
            "writable": false,
            "configurable": false,
            "enumerable": false
        });
        Object.defineProperty(this, "store", {
            "value": _store,
            "writable": false,
            "configurable": false,
            "enumerable": false
        });
    }
    save(callback) {
        this.store.set(clone(this.id), clone(this), this.cookie.maxAge, callback || noOp);
    }
    load(callback) {
        let self = this;

        this.store.get(clone(this.id), function onGet(err, session) {
            if (err) {
                overwriteSession(self, {});
            } else {
                overwriteSession(self, session);
            }

            return callback ? callback(err) : noOp(err);
        });
    }
    destroy(callback) {
        let self = this;

        this.store.destroy(clone(this.id), function onDelete(err) {
            self.id.renewSid();
            overwriteSession(self, {});

            return callback ? callback(err) : noOp(err);
        });
    }
}

module.exports = Session;
