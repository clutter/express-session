"use strict";

const uidSync = require("uid-safe").sync;

// assumes that tid/uid do not use "|"
const SID_LENGTH = 24;
const ID_SEPARATOR = "|";
const ID_SEPARATOR_REGEX = /^([^_]*)\|([^_]*)\|(.*)$/;

class Identity {
    constructor(tid, uid, sid) {
        this.tid = tid ? tid.toString() : "";
        this.uid = uid ? uid.toString() : "";
        this.sid = sid ? sid.toString() : Identity.generateSid();
    }
    renewSid() {
        this.sid = Identity.generateSid();
    }
    updateTid(tid) {
        this.tid = tid.toString();
        this.renewSid();
    }
    updateUid(uid) {
        this.uid = uid.toString();
        this.renewSid();
    }
    updateTidUid(tid, uid) {
        this.tid = tid.toString();
        this.uid = uid.toString();
        this.renewSid();
    }
    toString() {
        return [this.tid, this.uid, this.sid].join(ID_SEPARATOR);
    }
    static generateSid() {
        return uidSync(SID_LENGTH);
    }
    static fromString(str) {
        let args = {};
        let matches = str.match(ID_SEPARATOR_REGEX);

        if (matches) {
            args.tid = matches[1]; // eslint-disable-line no-magic-numbers
            args.uid = matches[2]; // eslint-disable-line no-magic-numbers
            args.sid = matches[3]; // eslint-disable-line no-magic-numbers
        }

        return new Identity(args.tid, args.uid, args.sid);
    }
}

module.exports = Identity;
