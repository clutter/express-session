"use strict";

var debug = require("debug")("express-session:Store");

const MAX_AGE_DEFAULT = 86400000; // milli-seconds in 1-day
var sessionData = [];

function findSessionElement(filter) {
    let data = sessionData.filter(filter);

    if (data.length !== 1) { // eslint-disable-line no-magic-numbers
        return new Error("error finding session");
    }

    return data[0]; // eslint-disable-line no-magic-numbers
}

function removeSessionElement(filter) {
    let idx = sessionData.findIndex(filter);

    if (idx < 0) { // eslint-disable-line no-magic-numbers
        return new Error("error finding session");
    }
    sessionData.splice(idx, 1); // eslint-disable-line no-magic-numbers

    return null;
}

function pruneTTL() {
    let now = Date.now();

    function ttl(data) {
        return now >= data.ttl;
    }
    while (removeSessionElement(ttl) === null) {/* eslint-dsable-line no-empty */} // jshint ignore:line
}


class Store {
    set(id, session, maxAge, callback) {
        let sess;

        function setFilter(data) {
            return id.sid === data.sid && id.uid === data.uid && id.tid === data.tid;
        }

        pruneTTL();
        sess = findSessionElement(setFilter);
        if (sess instanceof Error) {
            sessionData.push({
                "tid": id.tid,
                "uid": id.uid,
                "sid": id.sid,
                "ttl": Date.now() + (maxAge ? maxAge : MAX_AGE_DEFAULT),
                "session": session
            });
        } else {
            sess.ttl = Date.now() + (maxAge ? maxAge : MAX_AGE_DEFAULT);
            sess.session = session;
        }
        callback(null);
    }
    get(id, callback) {
        let sess;

        function getFilter(data) {
            return id.sid === data.sid && id.uid === data.uid && id.tid === data.tid;
        }

        pruneTTL();
        sess = findSessionElement(getFilter);
        if (sess instanceof Error) {
            debug("get - session not found: %s", id.toString());
            callback(sess);
        } else {
            callback(null, sess.session);
        }
    }
    destroy(id, callback) {
        function destroyFilter(data) {
            return id.sid === data.sid && id.uid === data.uid && id.tid === data.tid;
        }
        pruneTTL();
        if (removeSessionElement(destroyFilter) instanceof Error) {
            debug("destroy - session not found: %s", id.toString());
        }
        callback(null);
    }
    destroyAllUserSessions(uid, callback) {
        function matchUser(data) {
            return uid === data.uid;
        }

        while (removeSessionElement(matchUser) === null) {/* eslint-dsable-line no-empty */} // jshint ignore:line
        callback(null);
    }
    destroyAllTenantSessions(tid, callback) {
        function matchTenant(data) {
            return tid === data.tid;
        }

        while (removeSessionElement(matchTenant) === null) {/* eslint-dsable-line no-empty */} // jshint ignore:line
        callback(null);
    }
    destroyAllSessions(callback) {
        function matchAll() {
            return true;
        }

        while (removeSessionElement(matchAll) === null) {/* eslint-dsable-line no-empty */} // jshint ignore:line
        callback(null);
    }
}

module.exports = Store;
